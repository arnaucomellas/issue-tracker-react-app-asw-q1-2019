import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import IssueList from "./components/issues/IssueList";
import Issue from "./components/issues/Issue";
import IssueForm from "./components/issues/IssueForm";
import 'bootstrap/dist/css/bootstrap.css';

class App extends Component {
  render() {
    return (
        <Router>
          <div className="App">
            <ul>
              <li>
                <Link to="/issues">Issues</Link>
              </li>
              <li>
                <Link to="/issues/1">Issue 1</Link>
              </li>
              <li>
                <Link to="/issues/1/edit">Edit Issue 1</Link>
              </li>
              <li>
                <Link to="/issues/new">New Issue</Link>
              </li>
            </ul>

            <hr />

            <Switch>
              <Route path="/issues/:id/edit" key="edit-issue" component={IssueForm} />
              <Route path="/issues/new" key="new-issue" component={IssueForm} />
              <Route path="/issues/:id" component={Issue} />
              <Route path="/issues" component={IssueList} />
            </Switch>
          </div>
        </Router>
    );
  }
}

export default App;
