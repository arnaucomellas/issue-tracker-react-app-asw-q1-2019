import React from 'react'
import axios from 'axios'

import IssuePrioritySelector from "./IssuePrioritySelector";
import IssueTypeSelector from "./IssueTypeSelector";
import IssueStatusSelector from "./IssueStatusSelector";
import UserSelector from "./UserSelector";
import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";

class IssueForm extends React.Component{

    constructor(props) {
        super(props);
        if(props.initialState){
                this.state = props.initialState;
        }
        else{
            this.state = {...this.getDefaultState(), ...{id: props.initialId ? props.initialId : undefined}}
        }

        this.handleSelectorChange = this.handleSelectorChange.bind(this);

        this.handleTitleChange = this.handleTitleChange.bind(this)
        this.handleAssigneeChange = this.handleAssigneeChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this)

    }

    getDefaultState(){
        return ({
            id: undefined,
            title: "",
            assignee_id: "1",
            priority: "Major",
            issue_type: "Bug",
            status: "New",
            creator_id: "",
            description: "",
        });
    }

    handleTitleChange(event){
        this.setState({title: event.target.value});
    }

    handleDescriptionChange(event){
        this.setState({description: event.target.value});
    }

    handleAssigneeChange(assignee_id){
        this.setState({assignee_id: assignee_id});
    }

    handleSelectorChange(attribute, value){
        var newState = {};
        newState[attribute] = value;
        this.setState(newState, function () {
            console.log(this.state);
        });
    }
    componentDidMount() {
        const {match: {params}} = this.props;
        if(params.id){
            this.setState({id: params.id });
            axios.get('/api/v1/issues/'+params.id.toString())
                .then(response => {
                    this.setState(response.data)
                })
                .catch(error => console.log(error));
        }
        else{
            this.setState({...{id:undefined},...this.getDefaultState()})
        }
    }

    handleSubmit(action, method,e){
        var data;
        data = this.state;
        e.preventDefault();
        if (method == "PUT") {
            data = (({title, assignee_id, priority, status, issue_type, description }) => ({title, assignee_id, priority, status, issue_type, description}))(this.state)
        }
        axios({
            method: method,
            url: action,
            data: {issue:data}
        }).then(() => this.setState({ redirect: true }));
    }

    render(){

        const { redirect } = this.state;
        if (redirect) {
            return <Redirect to="/issues" />;
        }

        var h1, action, method;
        if(this.state.id != undefined){
            h1 = "Issue " + this.state.id.toString();
            action = `/api/v1/issues/${this.state.id}`;
            method = "PUT"
        }
        else{
            h1 = "New issue";
            action = `/api/v1/issues`;
            method = "POST"
        }
        return(
            <div className="container">
						      <h1>{h1}</h1>
						      <form onSubmit={e => this.handleSubmit(action,method,e)}>
							      <div className="form-group">
								      <label for="exampleFormControlInput1">
									      Title
									      <input type="text" className="form-control" id="exampleFormControlInput1" value={this.state.title} onChange={this.handleTitleChange}/>
								      </label>
							      </div>
							      <div className="form-group">
								      <label for="exampleFormControlTextarea1">
									      Description
									      <textarea className="form-control" id="exampleFormControlTextarea1" rows="3" value={this.state.description} onChange={this.handleDescriptionChange}/>
								      </label>
							      </div>
							      <div className="form-group">
								      <IssueTypeSelector initialIssueType={this.state.issue_type} handler={this.handleSelectorChange}/>
							      </div>
							      <div className="form-group">
								      <IssuePrioritySelector initialPriority={this.state.priority} handler={this.handleSelectorChange}/>
							      </div>
							      <div className="form-group">
								      <IssueStatusSelector initialStatus={this.state.status} handler={this.handleSelectorChange}/>
							      </div>
							      <div className="form-group">
							            <UserSelector name="assignee_id" initialValue={this.state.assignee_id} handler={this.handleSelectorChange}/>
							      </div>
                                  <div className="row">
                                      <div className="col-xs-6 p-3">
                                          <button type="submit" className="btn btn-primary">Submit</button>
                                      </div>
                                      <div className="col-xs-6 p-3">
                                          <Link to={`/issues`}>
                                              <button type="submit" className="btn btn-primary">Cancel</button>
                                          </Link>
                                      </div>
                                  </div>
						      </form>
            </div>
        );
    }

}

export default IssueForm;
