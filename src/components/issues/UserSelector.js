import React from 'react'
import axios from "axios";

class UserSelector extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            value: props.initialValue ? props.initialValue : undefined,
            users: props.initialUsers ? props.initialUsers : []
        }
        this.handleOnChange = this.handleOnChange.bind(this);
    }

    componentDidMount() {
        axios.get('/api/v1/users')
            .then(response => {
                this.setState({users: response.data})
            })
            .catch(error => console.log(error))
    }

    handleOnChange(event){
        var callback = function(){
            this.props.handler(this.props.name,this.state.value);
        };
        this.setState({value: event.target.value},callback);
    }

    render(){
        var items = this.state.users.map((item,index)=>{
            return(<option value={item.id} name="user">{item.username}</option>);
        });
        return(
            <label htmlFor="exampleFormControlSelect1">
                Assignee
                <select value={this.state.value} className="form-control" id="exampleFormControlSelect1" onChange={this.handleOnChange}>
                    {items}
                </select>
            </label>
        );
    }
}

export default UserSelector;
