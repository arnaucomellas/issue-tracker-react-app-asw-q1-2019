import React from 'react'

class IssueTypeSelector extends React.Component{

    constructor(props) {
        super(props);
        this.possibleTypes = ["Bug", "Enhancement", "Proposal", "Task"];
        this.state = {
            issueType: props.initialIssueType ? props.initialIssueType : "Bug"
        }
        this.handleOnChange = this.handleOnChange.bind(this)
    }

    handleOnChange(event){
        var callback = function(){
            this.props.handler("issue_type",this.state.issueType);
        };
        this.setState({issueType: event.target.value},callback);
    }

    render(){

        var options = this.possibleTypes.map((item, index)=>{
            return(<option value={item}>{item}</option>);
        });

        return(
            <label for="exampleFormControlSelect1">
				      Type
                <select className="form-control" id="exampleFormControlSelect1" value={this.state.issueType} onChange={this.handleOnChange}>
                    {options}
                </select>
            </label>
        );
    }

}

export default IssueTypeSelector;
