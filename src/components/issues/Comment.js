import React from 'react'
import axios from "axios";
import {Link} from "react-router-dom";

class Comment extends React.Component {

    constructor(props) {
        super(props);
        if(props.initialState){
            this.state = props.initialState;
        }
        else{
            var comment = props.initialComment ? props.initialComment : {
                id: props.initialId ? props.initialId : undefined,
                content: props.initialComment ? props.initialComment : "",
                user: props.initialUser ? props.initialUser : ""
            };
            this.state = {
                comment: comment,
                editing: false
          }
      }
        this.handleDeleteComment = this.handleDeleteComment.bind(this);
        this.handleSave = this.handleSave.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleDeleteComment(e){
        e.preventDefault();
        axios({
            method: "DELETE",
            url: `/api/v1/microposts/${this.state.comment.id}`,
            data: {micropost:{
                    id: this.state.comment.id
            }}
        }).then(this.props.handleReload);
    }

    handleSave(){
        axios({
            method: "PUT",
            url: `/api/v1/microposts/${this.state.comment.id}`,
            data: {micropost:{content: this.state.comment.content}}
        }).then(() =>  this.setState({editing: false}) );

    }

    handleEdit(){
        this.setState({editing: true})
    }

    handleChange(e){
        var { comment } = this.state;
        comment.content = e.target.value;
        this.setState({comment: comment})
    }

    render(){
        var content;
        if(this.state.editing){
            content = (
                <div>
                    <textarea value={this.state.comment.content} onChange={this.handleChange}></textarea>
                    <div className="row">
                        <div className="col-xs-6 p-3">
                            <button type="button" onClick={this.handleSave}> Save</button>
                        </div>
                        <div className="col-xs-6 p-3">
                            <form onSubmit={this.handleDeleteComment}>
                                <button type="submit"> Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }
        else{
            content = (
                <div>
                    <p>- {this.state.comment.content}</p>
                    <div className="row">
                        <div className="col-xs-6 p-3">
                            <button type="button" onClick={this.handleEdit}> Edit</button>
                        </div>
                        <div className="col-xs-6 p-3">
                            <form onSubmit={this.handleDeleteComment}>
                                <button type="submit"> Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
            );
        }

        return(
			<div>
				<p><b>{this.state.comment.user}</b></p>
                {content}


			</div>
        );
    }
}

export default Comment;
