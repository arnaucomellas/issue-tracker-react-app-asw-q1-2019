import React from 'react'

import Comment from "./Comment";

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useParams
} from "react-router-dom";
import axios from "axios";


class Issue extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: props.initialId ? props.initialId : undefined,
            title: props.initialTitle ? props.initialTitle : "",
            assignee: props.initialAssigne ? props.initialAssigne : "",
            priority: props.initialPriority ? props.initialPriority : "",
            issue_type: props.initialIssueType ? props.initialIssueType : "",
            status: props.initialStatus ? props.initialStatus : "",
            creator: props.initialCreator ? props.initialCreator : "",
            description: props.initialDescription ? props.initialDescription : "",
            votes: props.initialVotes ? props.initialVotes : "",
            watchers: props.initialWatches ? props.initialWatches : "",
            content: "",
            comments: (props.initialComments ? props.initialComments : [])
        }

        this.handleVoteWatch = this.handleVoteWatch.bind(this);
        this.handleContentChange = this.handleContentChange.bind(this);
        this.handleNewComment = this.handleNewComment.bind(this);
        this.loadIssue = this.loadIssue.bind(this);
        this.loadComments = this.loadComments.bind(this);
    }

    handleContentChange(event){
        this.setState({content: event.target.value});
    }

    handleNewComment(e){
        e.preventDefault();
        axios({
            method: "post",
            url: '/api/v1/microposts',
            data: {micropost:{
                    issue_id: this.state.id,
                    content: this.state.content
                }}
        }).then(response => {
            this.setState({comments: response.data, content: ""})
        });
    }

    componentDidMount() {
        const {match: {params}} = this.props;
        this.setState({id: params.id });
        this.loadIssue(params.id);
        this.loadComments(params.id);
    }

    loadIssue(id){
        axios.get('/api/v1/issues/'+id.toString())
            .then(response => {
                this.setState(response.data)
            })
            .catch(error => console.log(error));
    }

    loadComments(issueId){
        axios.get('/api/v1/microposts/?issue_id='+issueId.toString())
            .then(response => {
                this.setState({comments: response.data})
            })
            .catch(error => console.log(error))
    }

    getComments(id) {
        axios.get('/api/v1/microposts/?issue_id='+id.toString())
            .then(response => {
                this.setState({comments: response.data})
            })
            .catch(error => console.log(error));
    }

    handleVoteWatch(action, e){
        e.preventDefault();
        axios({
            method: "post",
            url: action
        }).then(response =>{
            this.setState(response.data)
        }).catch(reason => {
            if(reason.response.data.message) window.alert(reason.response.data.message);
            console.log(reason);
        });
    }

    render(){
      var comments = this.state.comments.map((item,index) => {
            return(
                <Comment key={item.id} initialComment={item} handleReload={()=>{this.loadComments(this.state.id)}}></Comment>
            );
        });

        return(
          <div className="container">
				    <div className="row">
					    <div className="col-9 p-3">
						    <h3>{this.state.title}</h3>
						    <p>Issue #{this.state.id} <p className="badge badge-success">{this.state.status}</p></p>
                            <p>This issue was created by {this.state.creator}.</p>
						    <hr/>
						    <h4>Comments</h4>
						    {comments}
                            <form onSubmit={this.handleNewComment}>
                                <textarea className="form-control" id="exampleFormControlTextarea1" rows="3" value={this.state.content} onChange={this.handleContentChange}/>
                                <button type="submit"> New Comment</button>
                            </form>
					    </div>
					    <div className="col-3 text-center border border-dark justify-content-center m-auto">
                            <p><b>Assignee </b>{this.state.assignee}</p>
                            <p><b>Priority </b>{this.state.priority}</p>
						    <p><b>Status </b>{this.state.status}</p>
						    <p><b>Type </b>{this.state.issue_type}</p>
						    <p><b>Votes </b>{this.state.votes}</p>
                            <div className="row justify-content-center mb-2">
                                <div className="col-xs-6 px-1 mt-0">
                                    <form onSubmit={e => this.handleVoteWatch(`/api/v1/issues/${this.state.id}/vote`,e)}>
                                        <button type="submit"> Vote</button>
                                    </form>                                </div>
                                <div className="col-xs-6 px-1">
                                    <form onSubmit={e => this.handleVoteWatch(`/api/v1/issues/${this.state.id}/unvote`,e)}>
                                        <button type="submit"> Unvote</button>
                                    </form>
                                </div>
                            </div>
						    <p><b>Watches </b>{this.state.watchers}</p>
                            <div className="row justify-content-center mb-2">
                                <div className="col-xs-6 px-2">
                                    <form onSubmit={e => this.handleVoteWatch(`/api/v1/issues/${this.state.id}/watch`,e)}>
                                        <button type="submit"> Watch</button>
                                    </form>                               </div>
                                <div className="col-xs-6 px-2">
                                    <form onSubmit={e => this.handleVoteWatch(`/api/v1/issues/${this.state.id}/unwatch`,e)}>
                                        <button type="submit"> UnWatch</button>
                                    </form>
                                </div>
                            </div>
					    </div>
				    </div>
          </div>
        );
    }
}

export default Issue;
