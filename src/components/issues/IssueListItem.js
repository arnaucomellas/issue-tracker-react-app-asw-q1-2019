import React from 'react'

import {Link, Redirect} from "react-router-dom";
import axios from "axios";


class IssueListItem extends React.Component {

    constructor(props) {
        super(props);
        if(props.initialState){
            this.state = props.initialState;
        }
        else{
            this.state = {
                id: props.initialId ? props.initialId : undefined,
                title: props.initialTitle ? props.initialTitle : "",
                assignee_name: props.initialAssigneeName ? props.initialAssigneeName : "",
                priority: props.initialPriority ? props.initialPriority : "",
                issue_type: props.initialIssueType ? props.initialIssueType : "",
                status: props.initialStatus ? props.initialStatus : "",
                creator_name: props.initialCreatorName ? props.initialCreatorName : "",
                redirect: false
            }
        }
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete(e){
        e.preventDefault();
        axios({
            method: "DELETE",
            url: `/api/v1/issues/${this.state.id}`,
        }).then(this.props.handleReload);
    }

    render(){
        return(
            <tr>
                <td>#{this.state.id}: {this.state.title}</td>
                <td>{this.state.assignee}</td>
                <td>{this.state.priority}</td>
                <td>{this.state.issue_type}</td>
                <td>{this.state.status}</td>
                <td>{this.state.creator}</td>
				        <td><Link to={`/issues/${this.state.id}`}>
						        <button type="button"> Show Issue</button>
					        </Link>
				        </td>
				        <td><Link to={`/issues/${this.state.id}/edit`}>
						        <button type="button"> Edit Issue</button>
					        </Link>
				        </td>
				        <td><form onSubmit={this.handleDelete}>
						        <button type="submit"> Delete Issue</button>
					        </form>
				        </td>
            </tr>
        );
    }
}

export default IssueListItem;
