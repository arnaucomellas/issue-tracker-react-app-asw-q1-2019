import React from 'react'

class IssueStatusSelector extends React.Component{

    constructor(props) {
        super(props);
        this.possibleStatus =  ["New", "Open", "On hold", "Resolved", "Duplicate", "Invalid", "Won't fix", "Closed"];
        this.state = {
            issueStatus: props.initialStatus ? props.initialStatus : "New"
        }
        this.handleOnChange = this.handleOnChange.bind(this)
    }

    handleOnChange(event){
        var callback = function(){
            this.props.handler("status",this.state.issueStatus);
        };
        this.setState({issueStatus: event.target.value},callback);
    }

    render(){

        var options = this.possibleStatus.map((item, index)=>{
            return(<option value={item}>{item}</option>);
        });

        return(
            <label for="exampleFormControlSelect1">
				      Status
                <select className="form-control" id="exampleFormControlSelect1" value={this.state.issueStatus} onChange={this.handleOnChange}>
                    {options}
                </select>
            </label>
        );
    }

}

export default IssueStatusSelector;
