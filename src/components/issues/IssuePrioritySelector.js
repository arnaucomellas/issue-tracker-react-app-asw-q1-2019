import React from 'react'

class IssuePrioritySelector extends React.Component{

    constructor(props) {
        super(props);
        this.possiblePriorities = ["Trivial", "Minor", "Major", "Critical", "Blocker"];
        this.state = {
            issuePriority: props.initialPriority ? props.initialPriority : "Major"
        }
        this.handleOnChange = this.handleOnChange.bind(this)
    }


    handleOnChange(event){
        var callback = function(){
            this.props.handler("priority",this.state.issuePriority);
        };
        this.setState({issuePriority: event.target.value},callback);
    }


    render(){

        var options = this.possiblePriorities.map((item, index)=>{
            return(<option value={item}>{item}</option>);
        });

        return(
            <label for="exampleFormControlSelect1">
				      Priority
                <select className="form-control" id="exampleFormControlSelect1" value={this.state.issuePriority} onChange={this.handleOnChange}>
                    {options}
                </select>
            </label>
        );
    }

}

export default IssuePrioritySelector;
