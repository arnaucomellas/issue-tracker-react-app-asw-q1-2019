import React from 'react'
import axios from 'axios'

import IssueListItem from "./IssueListItem";
import {Link} from "react-router-dom";

class IssueList extends React.Component {

    constructor(props) {
        super(props);
        if(props.initialState){
            this.state = props.initialState;
        }
        else{
            this.state = {
                items: (props.initialItems ? props.initialItems : [])
            }
        }
        this.getIssues = this.getIssues.bind(this)
    }

    getIssues() {
        axios.get('/api/v1/issues')
            .then(response => {
                this.setState({items: response.data})
            })
            .catch(error => console.log(error))
    }

    componentDidMount() {
        this.getIssues()
    }


    render() {
        var items = this.state.items.map((item,index) => {
            return(
                <IssueListItem key={item.id} initialState={item} handleReload={this.getIssues}></IssueListItem>
            );
        });

        return(

          <div className="container">
            <h1>Issues</h1>
            <table className="table table-hover table-sm">
					    <thead className="thead-light">
						    <tr>
							    <th>Title</th>
							    <th>Assignee</th>
							    <th>Priority</th>
							    <th>Type</th>
							    <th>Statue</th>
							    <th>Creator</th>
						    </tr>
					    </thead>
						  {items}
            </table>
                  <Link to="/issues/new">
                      <button type="button"> New Issue</button>
                  </Link>
          </div>

        );
    }

}

export default IssueList;
